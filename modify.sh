#!/bin/bash

CONTAINER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mysqltest)
echo "INSERT INTO foo.myUsers (name) VALUES ('Gipsz Jakab');" | mysql -h "$CONTAINER_IP" -uroot -ptest
echo "New row has been added to table myUsers."