#!/bin/bash

docker stop mysqltest
docker rm mysqltest
docker run --name=mysqltest --detach=true mysqltest
