#!/bin/bash

CONTAINER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mysqltest)
watch --interval 1 "echo 'SELECT COUNT(*) AS number_of_rows FROM myUsers;' | mysql -h $CONTAINER_IP -D foo -uroot -ptest"
