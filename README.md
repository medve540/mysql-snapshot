Fast restore of MySQL data
==========================

The goal is to reset MySQL data to a pre-defined state as quiclk as possible.

Steps to test
-------------

1. Build the Docker image containing MySQL

  `./build.sh`

2. Create a Docker container form it and populate it with the initial data (see dump.sql)

  `./init.sh`

3. Start the container and watch the data in it (print the number of lines in the myUsers table)

  `docker start mysqltest && ./waich.sh`

4. Open a new shell and modify the data by adding a new record to the test table

  `./modify`

5. Restore the MySQL data to the initial state

  `time ./reset.sh`
