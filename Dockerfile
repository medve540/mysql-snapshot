FROM mysql

ENV MYSQL_ROOT_PASSWORD=test
ADD mysqld.cnf /etc/mysql/conf.d/
ADD dump.sql /docker-entrypoint-initdb.d/dump.sql
ADD mysqld.sh /usr/local/bin/
RUN mkdir -p /var/mysql && \
    sed -Ei 's/^exec.+//' /usr/local/bin/docker-entrypoint.sh && \
    docker-entrypoint.sh mysqld
ENTRYPOINT ["mysqld.sh"]

